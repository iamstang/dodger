var director = cc.Director();
var GameMenu = cc.LayerColor.extend({
    ctor: function() {
        this._super(new cc.Color(0, 0, 0, 255));
        this.init();

    },
    init: function () {
        this.logo = cc.LabelTTF.create('0', 'Arial', 100);
        this.logo.setPosition(new cc.Point(SCREEN.WIDTH / 2, (SCREEN.HEIGHT / 2) + 100));
        this.logo.setString("dodger123");
        this.addChild(this.logo);

        this.playBtn = new cc.MenuItemFont("Start", play);

        this.playBtn.setPosition(new cc.Point(SCREEN.WIDTH / 2, ((SCREEN.HEIGHT / 2) - 50)));


        this.menu = new cc.Menu(this.playBtn);
        this.menu.setPosition(0,0);
        this.addChild(this.menu);

        return true;
    },
});

var play = function() {
    console.log('Game Start');
    cc.director.runScene(new StartScene());
}


GameMenu.scene = function() {
    var scene = new cc.Scene();
    var layer = new GameMenu();
    scene.addChild(layer);
    return scene;
};

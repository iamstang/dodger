var Enemy = cc.Sprite.extend({
	ctor : function() {
	this._super();
	this.initWithFile( 'res/images/ship.png' );

	var maxSpeed = 5;
	var minSpeed = 1;
	this.veloX = this.randomNum( minSpeed , maxSpeed );
	this.veloY = this.randomNum( minSpeed , maxSpeed);

  this.initPosition();

  },

  initPosition: function(){
    var posX = this.randomNum(0,SCREEN.WIDTH);

    while (posX > 100 && posX < SCREEN.WIDTH - 100){

      posX = this.randomNum(0,SCREEN.WIDTH);
    }

  	var posY = this.randomNum(0,SCREEN.HEIGHT);

    while ( posY > 100 && posY < SCREEN.HEIGHT - 100){
      posY = this.randomNum(0,SCREEN.HEIGHT);
    }

  	this.setPosition( new cc.p ( posX , posY ));
  },

	update: function( dt ){

	var pos = this.getPosition();
	if ( pos.x + this.veloX > screenWide || pos.x + this.veloX < 0 ){
		this.veloX = this.veloX * -1;
	}

	if ( pos.y + this.veloY > screenHeight || pos.y + this.veloY < 0 ){
		this.veloY = this.veloY * -1;
	}
	this.setPosition( new cc.p ( pos.x + this.veloX , pos.y + this.veloY ));
  rotationAngle += rotationRate;
  this.setRotation(rotationAngle);
	this.scheduleUpdate();
  },

	randomNum : function(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
  },

	getXpos : function(){
	var pos = this.getPosition();
	return pos.x;
  },

	getYpos : function(){
	var pos = this.getPosition();
	return pos.y;
  },

});

var rotationAngle = 0;
var rotationRate = 0.04;

var Gold = cc.Sprite.extend({
	ctor: function(){
	this._super();
	this.initWithFile( 'res/images/gold.png' );
},

	randomPosition: function(){
	this.setPosition( new cc.Point( 1+Math.floor(Math.random()*SCREEN.WIDTH) , 1+Math.floor(Math.random()*SCREEN.HEIGHT) ));
},

	closeTo: function( obj ) {
	var myPos = this.getPosition();
	var oPos = obj.getPosition();
	return ( ( Math.abs( myPos.x - oPos.x ) <= 35 ) && ( Math.abs( myPos.y - oPos.y ) <= 35 ) );
}
});

var GameLayer = cc.LayerColor.extend({
	init: function() {
	this._super( new cc.Color( 0, 0 , 0, 255 ) );
	this.setPosition( new cc.Point( 0, 0 ) );

	this.startGame();

	this.addKeyboardHandlers();
	this.addMouseHandlers();
	this.scheduleUpdate();

	return true;
},


	startGame : function(){

	  this.state = GameLayer.STATES.STARTED;

	  this.bg = new cc.Sprite( 'res/images/starfield.png' );
	  this.addChild( this.bg );
	  this.bg.setPosition( new cc.p( SCREEN.WIDTH / 2 , SCREEN.HEIGHT / 2));

	  this.player = new Player();
	  this.player.setPosition( cc.p ( SCREEN.WIDTH / 2 , SCREEN.HEIGHT / 2 ));
	  this.addChild( this.player );

	  this.gold = new Gold();
	  this.gold.setPosition( new cc.Point( 1 + Math.floor( Math.random() * SCREEN.WIDTH) , 1 + Math.floor( Math.random() * SCREEN.HEIGHT) ));
	  this.addChild( this.gold );


	  this.levelLabel = cc.LabelTTF.create( " Level : 1" , 'Arial' , 30);
	  this.levelLabel.setPosition( new cc.p( 600 , 500 ));
	  this.addChild( this.levelLabel );

	  this.scoreLabel = cc.LabelTTF.create( " Score : 0" , 'Arial' , 30);
	  this.scoreLabel.setPosition( new cc.p( 200 , 500 ));
	  this.addChild( this.scoreLabel );

	  time = 300;
	  score = 0;
	  level = 1;
	  enemies = [];

	  for (var i = 0 ; i < 6 ; i++ ){
	    enemies.push(new Enemy());
		  this.addChild(enemies[i]);
		  enemies[i].scheduleUpdate();
	  }

	  this.addMouseHandlers();
  },

	endGame : function(){
	  this.endLabel = cc.LabelTTF.create( " Game Over Score = "+ score +"\n        Click to restart", 'Arial' , 30);
	  this.endLabel.setPosition( new cc.p( 400 , 300 ));
	  this.addChild( this.endLabel );
  },

  checkCollition : function(){
	  var playerPos = this.player.getPosition();
	  for ( var i = 0 ; i < enemies.length ; i++ ){

		  if ( Math.abs(playerPos.x - enemies[i].getXpos()) < 30 && Math.abs(playerPos.y - enemies[i].getYpos()) < 30 ){
			  this.state = GameLayer.STATES.DEAD;
			  this.endGame();
		  }
	  }
  },



  update: function(){

	  if (this.state != GameLayer.STATES.DEAD){
		  this.checkCollition();

		  if ( this.gold.closeTo( this.player )){
			  score += 300;
			  this.gold.randomPosition();
		  }

		  if(time <= 0){

		    time += 300;
			  this.addEnemy();
			  level += 1;
		  } else{
			  time -= 1;
			  score +=1;
		  }

		  this.scoreLabel.setString( "Score : " + Math.floor(score));
		  this.levelLabel.setString( "Level : " + Math.floor(level));
	  }
  },

  addEnemy: function(){

    for (var i = 0 ; i < level ; i++){
      enemies.push(new Enemy());
      this.addChild(enemies[enemies.length -1]);
      enemies[enemies.length - 1 ].scheduleUpdate();
    }
  },

	addKeyboardHandlers: function() {
	  var self = this;
	  cc.eventManager.addListener({
	  event: cc.EventListener.KEYBOARD,
		  onKeyPressed : function( keyCode, event ) {

	    },
		  onKeyReleased: function( keyCode, event ) {

	    }
	  }, this);
  },

	onMouseClick : function( event ){

	  if (this.state == GameLayer.STATES.DEAD){
		  this.removeAllChildren(true);
		  this.scheduleUpdate();
		  this.startGame();
	  }
  },

	addMouseHandlers: function() {
	  var player = this.player;
	  var self = this;
	  cc.eventManager.addListener({
		  event : cc.EventListener.MOUSE,

		  onMouseMove : function(event){
		    player.setPosition( new cc.p ( event.getLocationX() , event.getLocationY() ));
	    },

	    onMouseDown : function(event){
		    cc.log("click");
		    self.onMouseClick(event);
	    }

	  }, this);
  },

	getPlayer : function(){
	  return this.player;
  },

});

var StartScene = cc.Scene.extend({
	onEnter: function() {
	this._super();
	var layer = new GameLayer();
	layer.init();
	this.addChild( layer );
}
});

var time = 300;
var score = 0;
var level = 1;
var enemies = [];
var screenHeight = 600;
var screenWide = 800;

GameLayer.STATES = {
		FRONT: 1,
		STARTED: 2,
		DEAD: 3
};
